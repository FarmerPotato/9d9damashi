XDT99=/cygdrive/c/ti99/xdt99

ASM=$(XDT99)/xas99.py
DM=$(XDT99)/xdm99.py
CP=cp -p
AFLAGS=-I src,king -R -q -S

OBJ=k2.obj
SRC=src/k2.a99
MAIN=k2.a99
LIST=obj/k2.lst
DISK=k2.dsk


all: k2

SRCS= \
    src/9d9damashi.a99 \
    src/hoonos.a99 \
    src/k2.a99 \
    src/kscan.a99 \
    src/magellan.a99 \
    src/maze.a99 \
    src/music.a99 \
    src/sprites.a99 \
	src/title.a99 \
    src/fugue.a99 \
    src/coda.a99 \


k2: $(OBJ) $(DISK)
	$(CP)  $< $@
	$(DM)  -a $@ -f df80 $(DISK)

$(OBJ): $(SRCS)
	$(ASM) $(AFLAGS) $(MAIN) -S -L $(LIST) -o $(OBJ)

$(DISK):
	$(DM)  --initialize 360 $(DISK)

